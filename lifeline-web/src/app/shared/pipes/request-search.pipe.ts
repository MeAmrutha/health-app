import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'requestSearch', pure: false })
export class RequestSearchPipe implements PipeTransform {

  transform(value, args?): Array<any> {
    let searchText = new RegExp(args, 'ig');
    if (value) {
      return value.filter(request => {
        let flag = false;
        if (request.requestCategory) {
          flag = request.requestCategory.search(searchText) !== -1;
        }
        if (!flag && request.location) {
          flag = request.location.search(searchText) !== -1;
        }
        if (!flag && request.requestedBloodGroup) {
          flag = request.requestedBloodGroup.search(searchText) !== -1;
        }
        return flag;
      });
    }
  }

}

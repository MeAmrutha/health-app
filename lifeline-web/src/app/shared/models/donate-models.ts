
export class DonateRequest {
    id: number;
    username: string;
    password: string;  
    profile: any;
    work: any;
    contacts: any;
    social: any;
    settings: any;
  }
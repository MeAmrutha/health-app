import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators'; 
import { Router } from '@angular/router';
import { TokenService } from './token.service';

@Injectable()
export class ApiInterceptorService implements HttpInterceptor {

  constructor(private router: Router, private tokenService: TokenService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.tokenService.getToken();
    if(token){
      req = req.clone({
        setHeaders:{
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(req).pipe(
      tap(
          event => {
              if(event instanceof HttpResponse){
                  //api call success
                  // console.log('success in calling API : ', event);
              }
          },
          error => {
              if(error instanceof HttpErrorResponse){
                  //api call error
                  console.log('error in calling API : ', error);
                  if(error.status == 401){
                      this.router.navigate(['/login']);
                  }
              }
          }
      ));
  }
}

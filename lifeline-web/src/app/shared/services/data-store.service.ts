import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataStoreService {

  private userData$: BehaviorSubject<any> ;
  constructor() { 
    this.userData$ = new BehaviorSubject<any>(null);
  }

  getUserData(){
    return this.userData$.asObservable();
  }

  setUserData(userData: any){
    this.userData$.next(userData);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { 

  }

  login(params: any){
    console.log(params);
    let serviceUrl = `${environment.baseUrl}/login`;
      return this.http.post(serviceUrl, params);
  }

  getUserInfo(){
    let serviceUrl = `${environment.baseUrl}/user/get`;
    return this.http.get(serviceUrl);
  }

  signUp(params: any) {
    console.log(params);
    let serviceUrl = `${environment.baseUrl}/signup`;
      return this.http.post(serviceUrl, params);
  }

  updateUser(params: any) {
    console.log(params);
    let serviceUrl = `${environment.baseUrl}/user/update`;
      return this.http.post(serviceUrl, params);
  }
}

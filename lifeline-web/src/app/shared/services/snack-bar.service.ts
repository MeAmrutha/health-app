import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private _snackBar: MatSnackBar) { }

  showSuccess(message: string) {
    this._snackBar.open(message, 'Ok', {
      duration: 5000
    });
  }
  showError(message: string) {
    this._snackBar.open(message, 'OK');
  }
  //,{
   // verticalPosition: 'bottom', horizontalPosition: 'center'
  //}
}

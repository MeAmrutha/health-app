import { Injectable, Inject } from '@angular/core';
import { JwtHelper } from 'angular2-jwt';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  static TOKEN_KEY = 'token';
  private jwtHelper: JwtHelper;
  constructor() {
    this.jwtHelper = new JwtHelper();
  }

  getToken() {
    return sessionStorage.getItem(TokenService.TOKEN_KEY);
  }

  setToken(token: string) {
    sessionStorage.setItem('token', TokenService.TOKEN_KEY);
  }

  removeToken(){
    sessionStorage.removeItem(TokenService.TOKEN_KEY);
  }
  isTokenExpired() {
    let isInvalid = true;
    try {
      isInvalid = this.jwtHelper.isTokenExpired(this.getToken());
    } catch (e) {
      console.log("error while isTokenExpired check " + e);
    }
    return isInvalid;
  }

}

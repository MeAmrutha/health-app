import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.scss']
})
export class ContentHeaderComponent implements OnInit {
  @Input('icon') icon:any;
  @Input('closeIcon') closeIcon:any;
  @Input('title') title:any;
  @Input('desc') desc:any;
  @Input('hideBreadcrumb') hideBreadcrumb:boolean = false;
  @Input('hasBgImage') hasBgImage:boolean = false;
  @Input('class') class:any;
  @Input('iconClass') iconClass :any;
  @Input('src') src :any;

  constructor(private router: Router) { }

  ngOnInit() {
  }
  onClose() {
    this.router.navigate(['/']);
  }
}

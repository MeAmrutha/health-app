import { Menu } from './menu.model';

export const verticalMenuItems = [ 
    new Menu (1, 'Dashboard', '/', null, 'dashboard', null, false, 0),
    new Menu (2, 'My Requests', '/request', null, 'emoji_people', null, false, 0),
    new Menu (3, 'Donate', '/donate', null, 'escalator_warning', null, false, 0),
    new Menu (3, 'Messenger', '/chat', null, 'chat', null, false, 0),
    new Menu (4, 'Notifications', '/notification', null, 'notifications', null, false, 0),
    new Menu (5, 'Support', '/support', null, 'help', null, false, 0)
]


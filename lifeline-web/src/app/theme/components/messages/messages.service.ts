import {Injectable} from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class MessagesService {

    constructor(private http:HttpClient){

    }
    
    public getNotifications(){
        const  serviceUrl = `${environment.baseUrl}/notification/get`;
        return this.http.get(serviceUrl);
    }

    public getUnreadNotifications(){
        const  serviceUrl = `${environment.baseUrl}/notification/getUnread`;
        return this.http.get(serviceUrl);
    }

    public markAsRead(params){
        const  serviceUrl = `${environment.baseUrl}/notification/markAsRead`;
        return this.http.post(serviceUrl, params);
    }

}
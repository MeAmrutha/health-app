import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { Router } from '@angular/router';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { MessagesService } from './messages.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ MessagesService ]
})
export class MessagesComponent implements OnInit {  
  @ViewChild(MatMenuTrigger, {static: false}) trigger: MatMenuTrigger;
  public selectedTab:number=1;
  public messages:Array<Object>;
  public meetings:Array<Object>;
  public userData;

  constructor(private messagesService:MessagesService, private storeService: DataStoreService, private router: Router) {  
    
    this.storeService.getUserData().subscribe((data: any)=>{
      this.userData = data;
    });
    this.getUnReadNotifications();
  }

  getUnReadNotifications(){
    this.messagesService.getUnreadNotifications().subscribe(
      (res: any)=>{
          this.messages = res;
      },
      err=>{
        console.log(err);
      }
    );
  }

  ngOnInit() {
  }

  openMessagesMenu() {
    this.messages = [];
    this.router.navigate(['/notification']);
   // this.trigger.openMenu();
   // this.selectedTab = 0;
  }

  onMouseLeave(){
    this.trigger.closeMenu();
  }

  stopClickPropagate(event: any){
    event.stopPropagation();
    event.preventDefault();
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { TokenService } from 'src/app/shared/services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserMenuComponent implements OnInit {
  public userImage = "assets/img/users/user.jpg";
  public userData: any;
  constructor(private dataStoreService: DataStoreService, private tokenService: TokenService, private router: Router) { }

  ngOnInit() {
    this.dataStoreService.getUserData().subscribe((data)=>{
        this.userData = data;
    });
  }

  onLogout(){
    this.tokenService.removeToken();
    this.router.navigate(['/landing']);
  }
}

import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { emailValidator } from '../../utils/app-validators';
import { DataStoreService } from 'src/app/shared/services/data-store.service';

@Component({
  selector: 'app-top-info-content',
  templateUrl: './top-info-content.component.html',
  styleUrls: ['./top-info-content.component.scss']
})
export class TopInfoContentComponent implements OnInit {
  public userData: any;
  constructor(public formBuilder: FormBuilder, private dataStoreService: DataStoreService) { }

  ngOnInit() {
    this.dataStoreService.getUserData().subscribe((data)=>{
        this.userData = data;
    })
  }

}

import { Routes, RouterModule, PreloadAllModules  } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PagesComponent } from './pages/pages.component';
import { BlankComponent } from './pages/blank/blank.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
import { ErrorComponent } from './pages/errors/error/error.component';
import { UserResolverService } from './shared/services/user-resolver.service';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { NotificationComponent } from './pages/notification/notification.component';
import { ProfileComponent } from './pages/profile/profile.component';
//import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';

export const routes: Routes = [
    { 
        path: '', 
        component: PagesComponent, children: [
            { path: '', loadChildren: './pages/dashboard/dashboard.module#DashboardModule', data: { breadcrumb: 'Dashboard' } , canActivate: [AuthGuardService]},
            { path: 'support', loadChildren: './pages/users/users.module#UsersModule', data: { breadcrumb: 'Support' }, canActivate: [AuthGuardService] },
            { path: 'request', loadChildren: './pages/request/request.module#RequestModule', data: { breadcrumb: 'Requests' }, canActivate: [AuthGuardService] },
            { path: 'donate', loadChildren: './pages/donate/donate.module#DonateModule', data: { breadcrumb: 'Donate' }, canActivate: [AuthGuardService] },
            { path: 'chat', loadChildren: './pages/chat/chat.module#ChatModule', data: { breadcrumb: 'Chat' }, canActivate: [AuthGuardService] },
            { path: 'notification', component: NotificationComponent, data: { breadcrumb: 'Notifications' }, canActivate: [AuthGuardService] },
            { path: 'profile', component: ProfileComponent, data: { breadcrumb: 'Profile' }, canActivate: [AuthGuardService] },
            { path: 'blank', component: BlankComponent, data: { breadcrumb: 'Blank page' }, canActivate: [AuthGuardService] },
        ],
        resolve:{userData: UserResolverService}
    },
    //{ path: 'profile', component: EditProfileComponent, canActivate: [AuthGuardService] },
    { path: 'landing', loadChildren: './pages/landing/landing.module#LandingModule' },
    { path: 'login', loadChildren: './pages/login/login.module#LoginModule' },
    { path: 'register', loadChildren: './pages/register/register.module#RegisterModule' },
    { path: 'error', component: ErrorComponent, data: { breadcrumb: 'Error' } },
    { path: '**', component: NotFoundComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
   //preloadingStrategy: PreloadAllModules,  // <- comment this line for activate lazy load
    useHash: true
});
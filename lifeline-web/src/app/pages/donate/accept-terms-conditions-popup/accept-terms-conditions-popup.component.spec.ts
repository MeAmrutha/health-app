import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptTermsConditionsPopupComponent } from './accept-terms-conditions-popup.component';

describe('AcceptTermsConditionsPopupComponent', () => {
  let component: AcceptTermsConditionsPopupComponent;
  let fixture: ComponentFixture<AcceptTermsConditionsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptTermsConditionsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptTermsConditionsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

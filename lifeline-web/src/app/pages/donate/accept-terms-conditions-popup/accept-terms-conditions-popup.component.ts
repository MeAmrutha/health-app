import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-accept-terms-conditions-popup',
  templateUrl: './accept-terms-conditions-popup.component.html',
  styleUrls: ['./accept-terms-conditions-popup.component.scss']
})
export class AcceptTermsConditionsPopupComponent implements OnInit {
  public category:any;
  constructor(public dialogRef: MatDialogRef<AcceptTermsConditionsPopupComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  this.category = this.data;
  }

  onAgree() {
    this.dialogRef.close('Agree');
  }

  onCancel() {
    this.dialogRef.close('Cancel');
  }
}

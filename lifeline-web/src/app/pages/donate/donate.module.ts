import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DonateComponent } from './donate.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { AcceptTermsConditionsPopupComponent } from './accept-terms-conditions-popup/accept-terms-conditions-popup.component';

export const routes = [
  { path: '', component: DonateComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    DonateComponent,
    AcceptTermsConditionsPopupComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule    
  ],
  entryComponents:[AcceptTermsConditionsPopupComponent]
})
export class DonateModule { }

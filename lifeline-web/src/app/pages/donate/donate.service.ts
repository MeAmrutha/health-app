import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DonateService {
  constructor(private http: HttpClient) { }

  getDonateRequests(){
    const serviceUrl = `${environment.baseUrl}/donate_request/get`;
    return this.http.get(serviceUrl);
  }

  assignDonor(params) {
    const serviceUrl = `${environment.baseUrl}/donate_request/assign_donor`;
    return this.http.post(serviceUrl,params);
  }
}

import { Component, OnInit } from '@angular/core';
import { DonateService } from './donate.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { AcceptTermsConditionsPopupComponent } from './accept-terms-conditions-popup/accept-terms-conditions-popup.component';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss']
})
export class DonateComponent implements OnInit {
  public users: any[];
  public searchText: string;
  public page:any;
  public showSearch:boolean = false;
  public viewType:string = 'grid';
  public currentUserDetails;
  constructor(public dialog: MatDialog, private donateRequestService: DonateService, private userService: DataStoreService,
    private snackbar: SnackBarService
) { }

  ngOnInit() {
    
      this.userService.getUserData().subscribe((resp)=> {
        this.currentUserDetails = resp;
        this.getDonateRequest();
      },err => {

      })
  }
  public changeView(viewType){
    this.viewType = viewType;
    this.showSearch = false;
  }

  public onAcceptRequest(donateRequest) {
    let params = {
      donateRequestId: donateRequest.donateRequestId,
      donorId: this.currentUserDetails.userId,
    }
    let dialogRef = this.dialog.open(AcceptTermsConditionsPopupComponent, {
      width: 'auto',
      data:  donateRequest.requestCategory,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
     if(result === 'Agree') {
      this.donateRequestService.assignDonor(params).subscribe((res:any)=> {
        if(res.status) {
          this.getDonateRequest();
          this.snackbar.showSuccess("Thanks for accepting! Requestor will contact you soon");
        }
        else {
          console.log(res.message);
          this.snackbar.showError(res.message);
        }
      }, err=> {
        this.snackbar.showError(err);
  
      });
     }
    })
   
  }

 

  public getDonateRequest() {
    this.donateRequestService.getDonateRequests().subscribe(
      (response: any[])=>{
        this.users = response.filter((p:any)=>p.requestorId != this.currentUserDetails.userId);
      },error=>{
        this.snackbar.showError(error);
        console.log(error);

      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { DataStoreService } from 'src/app/shared/services/data-store.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public userData;
  constructor(private store: DataStoreService) { }

  ngOnInit() {
    this.store.getUserData().subscribe(data=> {
      this.userData = data;
    })
  }

}

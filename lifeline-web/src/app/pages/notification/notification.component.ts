import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { MessagesService } from 'src/app/theme/components/messages/messages.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  providers:[MessagesService]
})
export class NotificationComponent implements OnInit {

  @ViewChild(MatMenuTrigger, {static: false}) trigger: MatMenuTrigger;
  public selectedTab:number=0;
  public messages:Array<Object>;
  public userData: any;
  constructor(private messagesService:MessagesService, private store: DataStoreService) { 

    this.store.getUserData().subscribe(data=>{
        this.userData = data;
    });
    this.getNotifications(this.userData.userId);
  }

  getNotifications(userId){
    this.messagesService.getNotifications().subscribe((res: any)=>{
      if(res){
        this.messages = res;
        this.markAsRead();
      }
    }, error=>{

    });
  }

  markAsRead(){
    const notificationIds = this.messages.map((p:any)=>p.id.notificationId);
    this.messagesService.markAsRead(notificationIds).subscribe(res=>{
      console.log("markAsRead :" + res);
    }, err=>{
      console.log("Error while markAsRead "+ err);
    });
  }

  ngOnInit() {
  }

  
  stopClickPropagate(event: any){
    event.stopPropagation();
    event.preventDefault();
  }

}

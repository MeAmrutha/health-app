import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  public form:FormGroup;
  public settings: Settings;
  public errorMsg;
  public isProcessing = false;
  constructor(public appSettings:AppSettings, public fb: FormBuilder, public router:Router,
    private userService: UserService){
    this.settings = this.appSettings.settings; 
    this.form = this.fb.group({
      'username': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(7)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      'rememberMe': false
    });
  }

  public onSubmit(values:Object):void {
    if (this.form.valid) {    
      this.isProcessing = true;
      sessionStorage.removeItem('token');
      const params = values;
        this.userService.login(params).subscribe(
          (response: any)=>{
            this.isProcessing = false;
            if(response){
              sessionStorage.setItem('token', response.token);
              this.router.navigate(['/']);
            }
          },
          error=>{
            this.isProcessing = false;
              console.log("Error occured while login + "+ JSON.stringify(error));
              if(error.status){
                if(error.status == 401){
                    this.errorMsg = 'Invalid login credentials';
                }
              }
          }
        )
     
    }
  }

  ngAfterViewInit(){
    this.settings.loadingSpinner = false; 
  }
}
import { Injectable } from '@angular/core'
import { Chat } from './chat.model';

let date = new Date(),
    day = date.getDate(),
    month = date.getMonth(),
    year = date.getFullYear(),
    hour = date.getHours(),
    minute = date.getMinutes();

let chats = [
    new Chat(
        'assets/img/profile/user.jpg',
        'Amrutha Vadapalli', 
        'Online',
        'Thank you very much',
        new Date(year, month, day-2, hour, minute),
        false
    ),
    new Chat(
        'assets/img/profile/user.jpg',
        'Satya Talatam',
        'Do not disturb',
        'Thank you very much',
        new Date(year, month, day-2, hour, minute),
        false
    ),
    new Chat(
        'assets/img/profile/user.jpg',
        'Sudha Pallam',
        'Away',
        'Thank you very much',
        new Date(year, month, day-2, hour, minute),
        false
    ),
    new Chat(
        'assets/img/profile/user.jpg',
        'Vijay Anirudh',
        'Online',
        'Thank you very much',
        new Date(year, month, day-2, hour, minute),
        false
    ),
    new Chat(
        'assets/img/profile/user.jpg',
        'Sowmya Paluru',
        'Offline',
        'Thank you very much',
        new Date(year, month, day-2, hour, minute),
        false
    ),  
    new Chat(
        'assets/img/profile/user.jpg',
        'Akhil T',
        'Online',
        'Thank you very much',
        new Date(year, month, day-2, hour, minute),
        false
    )
]

let talks = [
    new Chat(
        'assets/img/profile/ashley.jpg',
        'Amrutha Vadapalli', 
        'Online',
        'Hi, I\'m looking for B+ve Plasma.  can you pls donate if possible?',
        new Date(year, month, day-2, hour, minute+3),
        false
    ),
    new Chat(
        'assets/img/users/user.jpg',
        'Emilio Verdines', 
        'Online',
        'Hi, I\'ll definitely donate',
        new Date(year, month, day-2, hour, minute+2),
        true
    )
]

@Injectable()
export class ChatService {

    public getChats():Array<Chat> {
        return chats;
    }

    public getTalk():Array<Chat> {
        return talks;
    }

}


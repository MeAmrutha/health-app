import { Component, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { LandingService } from './landing.service';
import { TokenService } from 'src/app/shared/services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  encapsulation: ViewEncapsulation.None 
})
export class LandingComponent {
  public menuItems;  
  public settings: Settings;  
  constructor(public appSettings:AppSettings, private landingService:LandingService, private tokenService: TokenService,
     private router: Router) {
    this.settings = this.appSettings.settings; 
  }

  ngOnInit(){
    if(!this.tokenService.isTokenExpired()){
        this.router.navigate(['/']);
        return;
    }
    this.menuItems = this.landingService.getMenuItems();    
  }

  ngAfterViewInit(){
    this.settings.loadingSpinner = false; 
  }
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { LandingService } from './landing.service';
import { LandingComponent } from './landing.component';
import { FeaturesComponent } from './features/features.component';

export const routes = [
  { path: '', component: LandingComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    SharedModule,
    ScrollToModule.forRoot()
  ],
  declarations: [
    LandingComponent,
    FeaturesComponent
  ],
  providers: [ 
    LandingService 
  ] 
})
export class LandingModule { }

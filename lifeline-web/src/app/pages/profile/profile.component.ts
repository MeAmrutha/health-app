import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';
import { UserService } from 'src/app/shared/services/user.service';
import { emailValidator } from 'src/app/theme/utils/app-validators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public form:FormGroup;
  currentUserDetails: any;
  public bloodGroupOptions = [
    {value: 'A+', viewValue: 'A+'},
    {value: 'A-', viewValue: 'A-'},
    {value: 'B+', viewValue: 'B+'},
    {value: 'B-', viewValue: 'B-'},
    {value: 'O+', viewValue: 'O+'},
    {value: 'O-', viewValue: 'O-'},
    {value: 'AB+', viewValue: 'AB+'},
    {value: 'AB-', viewValue: 'AB-'}
  ];
  public genderOptions = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'},
  ];

  isProcessing = false;
  constructor( public fb: FormBuilder, public router:Router, private store:DataStoreService,
    private userService:UserService,private snackbarService: SnackBarService) { }

  ngOnInit() {
   
    this.store.getUserData().subscribe((resp)=> {
      this.currentUserDetails = resp;
      this.intiForm();
    })
  }

  intiForm(){
    this.form = this.fb.group({
      'userId': [this.currentUserDetails.userId, ],
      'fullName': [this.currentUserDetails.fullName, Validators.compose([Validators.required, Validators.minLength(3)])],
      'email': [this.currentUserDetails.email, Validators.compose([Validators.required, emailValidator])],
      'phoneNumber': [this.currentUserDetails.phoneNumber, Validators.required],
      'bloodGroup': [this.currentUserDetails.bloodGroup],
      'gender': [this.currentUserDetails.gender],
      'location': [this.currentUserDetails.location]
    });
  }

  public onUpdate(values:any):void {
    if (this.form.valid) {
      this.isProcessing = true;
      this.userService.updateUser(values).subscribe(
        (response: any)=>{
          this.isProcessing = false;
          if(response){
           this.store.setUserData(response);
            this.snackbarService.showSuccess("Your profile updated successfully");
          }
          else{
            this.snackbarService.showError("Error while updating profile");
          }
        },
        error=>{
          this.isProcessing = false;
          this.snackbarService.showError("Error while updating profile "+ error);
        })
    }
  }

}

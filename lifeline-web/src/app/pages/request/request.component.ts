import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { RequestFormDialogComponent } from './request-form-dialog/request-form-dialog/request-form-dialog.component';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { RequestService } from './request.service';
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {
  public showSearch:boolean = false;
  public viewType:string = 'list';
  public currentUserDetails;
  public requestList: any[];
  public dialogRef:any;
  public donorDetails;
  public searchText:any;

  constructor(public dialog: MatDialog, private store: DataStoreService, private requestService: RequestService,
               public snackbarService: SnackBarService) { }

  ngOnInit() {
    this.store.getUserData().subscribe((resp)=> {
      this.currentUserDetails = resp;
    },err => {

    })
   this.getMyRequests();  
  }
  onDeleteRequest(req) {
    let params = {
      donateRequestId: req.donateRequestId,
      status: 'Deleted'
    }
    this.updateStatus(params);
  }
  onUpdateRequest(req){
    let dialogRef = this.dialog.open(RequestFormDialogComponent, {
      width: '500px',
      data:  {userInfo:this.currentUserDetails, request:req},
      disableClose: true

    });
    dialogRef.afterClosed().subscribe(result => {
     if(result === 'Closed') {
        this.getMyRequests();
     }
    });
  }
  openRequestFormDialog() {
      let dialogRef = this.dialog.open(RequestFormDialogComponent, {
        width: '500px',
        data:  {userInfo:this.currentUserDetails, request: null},
        disableClose: true

      });
      dialogRef.afterClosed().subscribe(result => {
       if(result === 'Closed') {
          this.getMyRequests();
       }
      });
  }

  getMyRequests() {
    this.requestService.getMyRequests().subscribe((resp: any[]) => {
      this.requestList = resp;
    }, err => {

    })
  }
  
  public onGotDonation(req) {
    let params = {
      donateRequestId: req.donateRequestId,
      status: 'Completed'
    }
    this.updateStatus(params);
  
  }

  public onNotRequired(req) {
    let params = {
      donateRequestId: req.donateRequestId,
      status: 'Cancelled'
    }
    this.updateStatus(params);
    
  }

  public onReopenRequest(req) {
    let params = {
      donateRequestId: req.donateRequestId,
      status: 'Pending'
    }
    this.updateStatus(params);
  }

  public updateStatus(params) {
    this.requestService.updateStatus(params).subscribe((res:any)=> {
      if(res.status) {
        let msg = '';
        if(params.status === 'Cancelled') {
          msg = 'Your request has been cancelled'
        }
        if(params.status === 'Completed') {
          msg = 'Happy to help you!!'

        }
        if(params.status === 'Deleted') {
          msg = 'Your request has been deleted'
        }
        if(params.status === 'Pending') {
          msg = 'Your request has been re-opened successfully'
        }
        this.snackbarService.showSuccess(msg);
        this.getMyRequests();
      } else {
        this.snackbarService.showError(res.message);
      }
    }, err=> {
      this.snackbarService.showError(err);
    })
  }

  public changeView(viewType){
    this.viewType = viewType;
    this.showSearch = false;
  }
}

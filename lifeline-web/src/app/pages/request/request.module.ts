import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/theme/pipes/pipes.module';
import { RequestComponent } from './request.component';
import { RequestFormDialogComponent } from './request-form-dialog/request-form-dialog/request-form-dialog.component';


export const routes = [
  { path: '', component: RequestComponent, pathMatch: 'full' }
];
@NgModule({
  declarations: [RequestComponent, RequestFormDialogComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SharedModule,
    PipesModule    
  ],
  entryComponents: [RequestFormDialogComponent]
})
export class RequestModule { }

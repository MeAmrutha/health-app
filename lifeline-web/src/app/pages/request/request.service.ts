import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private http:HttpClient) { }
  getMyRequests(){
    const serviceUrl = `${environment.baseUrl}/donate_request/get_my_requests`;
    return this.http.get(serviceUrl);
  }

  raiseNewRequest(params) {
    const serviceUrl = `${environment.baseUrl}/donate_request/save`;
    return this.http.post(serviceUrl,params);
  }

  
  updateRequest(params) {
    const serviceUrl = `${environment.baseUrl}/donate_request/update`;
    return this.http.post(serviceUrl,params);
  }


  deleteReques(donateRequestId) {
    const serviceUrl = `${environment.baseUrl}/donate_request/deleteRequest/${donateRequestId}`;
    return this.http.delete(serviceUrl);
  }

  updateStatus(params) {
    const serviceUrl = `${environment.baseUrl}/donate_request/updateStatus`;
    return this.http.post(serviceUrl,params);
  }
}

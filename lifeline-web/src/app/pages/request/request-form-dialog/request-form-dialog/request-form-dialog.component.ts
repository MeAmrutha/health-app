import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { RequestService } from '../../request.service';
import { RequestFormModel } from '../request-form-model'
import { SnackBarService } from 'src/app/shared/services/snack-bar.service';

@Component({
  selector: 'app-request-form-dialog',
  templateUrl: './request-form-dialog.component.html',
  styleUrls: ['./request-form-dialog.component.scss']
})
export class RequestFormDialogComponent implements OnInit {
  public form:FormGroup;
  public requestFormModel: RequestFormModel;
  public selected;
  public requestForOptions = [
    {value: 'Myself', viewValue: 'Myself'},
    {value: 'Others', viewValue: 'Others'}
  ]
  public requestCategoryOptions = [
    {value: 'Blood', viewValue: 'Blood'},
    {value: 'Plasma', viewValue: 'Plasma'},
    {value: 'Covid Recovered Plasma', viewValue: 'Covid Recovered Plasma'}

  ]
  public bloodGroupOptions = [
    {value: 'A+', viewValue: 'A+'},
    {value: 'A-', viewValue: 'A-'},
    {value: 'B+', viewValue: 'B+'},
    {value: 'B-', viewValue: 'B-'},
    {value: 'O+', viewValue: 'O+'},
    {value: 'O-', viewValue: 'O-'},
    {value: 'AB+', viewValue: 'AB+'},
    {value: 'AB-', viewValue: 'AB-'}
  ];
  errorMsg: any;
  isProcessing = false;

  constructor(public fb: FormBuilder, public router:Router, @Inject(MAT_DIALOG_DATA) public data: any,
              private requestService: RequestService,private snackbarService: SnackBarService,
              public dialogRef: MatDialogRef<RequestFormDialogComponent>){
    this.requestFormModel = new RequestFormModel();
    if(data){
      if(!data.request){
        this.requestFormModel.location = data.userInfo.location;
        this.requestFormModel.requestorPhNum = data.userInfo.phoneNumber;
      }
      else{
        this.requestFormModel = {...data.request};
      }
    }
    
    this.form = this.fb.group({
      'requestFor': [this.requestFormModel.requestFor, Validators.compose([Validators.required])],
      'requestCategory': [this.requestFormModel.requestCategory, Validators.compose([Validators.required])],
      'requestedBloodGroup': [this.requestFormModel.requestedBloodGroup],
      'requestorPhNum':[this.requestFormModel.requestorPhNum],
      'description': [this.requestFormModel.description],
      'location': [this.requestFormModel.location]
    });
  }
  ngOnInit() {

  }
  public submitRequest(values:RequestFormModel){
    console.log("data :2 :"+ JSON.stringify(this.requestFormModel));
    if(this.requestFormModel.donateRequestId){
      this.updateRequest(values)
    }
    else{
      this.raiseNewRequest(values);
    }
  }
  public raiseNewRequest(values:RequestFormModel):void {
    if (this.form.valid) { 
     this.setDataForNewRequest(values);
     this.isProcessing = true;
      this.requestService.raiseNewRequest(this.requestFormModel).subscribe((res:any)=>{
        this.isProcessing = false;
        if(res.status) {
          this.dialogRef.close('Closed');
          this.snackbarService.showSuccess(res.message);
        } else {
          this.snackbarService.showError(res.message);
        }
      },
        err=> {
          this.isProcessing = false;
          this.snackbarService.showError(err);
        });
    }     
  }

  public updateRequest(values:RequestFormModel):void {
    if (this.form.valid) { 
     this.setDataForNewRequest(values);
     this.isProcessing = true;
      this.requestService.updateRequest(this.requestFormModel).subscribe((res:any)=>{
        this.isProcessing = false;
        if(res.status) {
          this.dialogRef.close('Closed');
          this.snackbarService.showSuccess(res.message);
        } else {
          this.snackbarService.showError(res.message);
        }
      },
        err=> {
          this.isProcessing = false;
          this.snackbarService.showError(err);
        });
    }     
  }

  setDataForNewRequest(values) {
    this.requestFormModel = {...this.requestFormModel, ...values};
    this.requestFormModel.requestorId = this.data.userInfo.userId;
    this.requestFormModel.status = 'Pending';
    this.requestFormModel.deleted = false;
  }
  
}

export class RequestFormModel {
    public  donateRequestId;
	public  requestorId: String;
	public  userInfoDto: Object;
	public  requestFor: String
    public  requestCategory: String;
	public  requestedBloodGroup: String;
	public  requestorPhNum: String;
	public  description: String;
	public  additionalInfo: String;
	public  location: String;
	public  status: String;
	public 	requestedOn: Date; 
    public  donorId: String; 
    public  deleted:boolean;
}
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ UsersService ]  
})
export class UsersComponent implements OnInit {
    public users: any;
    public searchText: string;
    public page:any;
    public settings: Settings;
    public showSearch:boolean = false;
    constructor(public appSettings:AppSettings, 
                public dialog: MatDialog,
                public usersService:UsersService){
        this.settings = this.appSettings.settings; 
        this.users = [{userId: '2340404',fullName: 'Satya Talatam', email: 'satya.talatam@gmail.com', location: 'Hyderabad', gender: 'Male',  phone: '040-123456789'},
        
        {userId: '2350505',fullName: 'Amrutha Vadapalli', email: 'amrutha.vadapalli@gmail.com', location: 'Pune', gender: 'Female',  phone: '020-123456789'},
        {userId: '2360606',fullName: 'Soumya Paluru', email: 'soumya.paluru@gmail.com', location: 'Bangalore', gender: 'Female',  phone: '080-123456789'}
        ]
    }

    ngOnInit() {
      
    }

    public onPageChanged(event){
        this.page = event;
        document.getElementById('main').scrollTop = 0;
    }

  
}
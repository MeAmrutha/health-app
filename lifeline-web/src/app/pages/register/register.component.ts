import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { emailValidator, matchingPasswords } from '../../theme/utils/app-validators';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent {
  public form:FormGroup;
  public settings: Settings;
  public isProcessing: boolean = false;
  public phoneRegex = "^[6-9]{1}[0-9]{9}$";
  public bloodGroupOptions = [
    {value: 'A+', viewValue: 'A+'},
    {value: 'A-', viewValue: 'A-'},
    {value: 'B+', viewValue: 'B+'},
    {value: 'B-', viewValue: 'B-'},
    {value: 'O+', viewValue: 'O+'},
    {value: 'O-', viewValue: 'O-'},
    {value: 'AB+', viewValue: 'AB+'},
    {value: 'AB-', viewValue: 'AB-'}
  ];
  public genderOptions = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'},
  ]

  constructor(public appSettings:AppSettings, public fb: FormBuilder, public router:Router, private userService:UserService){

    this.settings = this.appSettings.settings; 
    this.form = this.fb.group({
      'userId': [null, Validators.compose([Validators.required, Validators.pattern('^[2]{1}[0-9]{6}$')])],
      'fullName': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'password': ['', Validators.required],
      'phoneNumber': [null, Validators.compose([Validators.required, Validators.pattern(this.phoneRegex)])],
      'bloodGroup': ['', Validators.required],
      'gender': ['', Validators.required],
      'location': ['', Validators.required]
    },
    );
  }

  public onSubmit(values:Object):void {
    if (this.form.valid) {
      this.isProcessing =true;
      sessionStorage.removeItem('token');
      const params = values;
        this.userService.signUp(params).subscribe(
          (response: any)=>{
            this.isProcessing =false;
            console.log("reponse "+ JSON.stringify(response));
            if(response){
              sessionStorage.setItem('token', response.token);
              this.router.navigate(['/']);
            }
          },
          error=>{
            this.isProcessing =true;
          })
    }
  }

  ngAfterViewInit(){
    this.settings.loadingSpinner = false; 
  }
}
package com.mphasis.healthappservice.util;

public class SecurityConstants {
	public static final String SECRET = "health_app";
	public static final long EXPIRATION_TIME = 60 * 60 * 24; // 1 day
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users/sign-up";
}

package com.mphasis.healthappservice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="donate_requests")
public class DonateRequestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="donate_request_id")
	private Long donateRequestId;
	
	@Column(name="requestor_id")
	private String requestorId;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "requestor_id", insertable = false,updatable = false)
	private UserEntity requestor;
	
	@Column(name="request_for")
	private String requestFor;
	
	@Column(name="requested_blood_group")
	private String requestedBloodGroup;
	
	@Column(name="request_category")
	private String requestCategory;
	
	@Column(name="description")
	private String description;
	
	@Column(name="additional_info")
	private String additionalInfo;
	
	@Column(name="location")
	private String location;
	
	@Column(name="status")
	private String status;
	
	@Column(name="requested_on")
	private Date requestedOn;
	
	@Column(name="donor_id")
	private String donorId;
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "donor_id", insertable = false,updatable = false, nullable = true)
	private UserEntity donor;
	
	@Column(name="is_active")
	private boolean isActive;
	
	@Column(name="is_deleted")
	private boolean isDeleted;
	
	@Column(name="requestor_phone_number")
	private String requestorPhNum;
	
	@Column(name="last_updated_on")
	private Date lastModifiedOn;

	public String getRequestorPhNum() {
		return requestorPhNum;
	}

	public void setRequestorPhNum(String requestorPhNum) {
		this.requestorPhNum = requestorPhNum;
	}

	public String getRequestedBloodGroup() {
		return requestedBloodGroup;
	}

	public void setRequestedBloodGroup(String requestedBloodGroup) {
		this.requestedBloodGroup = requestedBloodGroup;
	}

	public Long getDonateRequestId() {
		return donateRequestId;
	}

	public void setDonateRequestId(Long donateRequestId) {
		this.donateRequestId = donateRequestId;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestFor() {
		return requestFor;
	}

	public void setRequestFor(String requestFor) {
		this.requestFor = requestFor;
	}

	public String getRequestCategory() {
		return requestCategory;
	}

	public void setRequestCategory(String requestCategory) {
		this.requestCategory = requestCategory;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public String getDonorId() {
		return donorId;
	}

	public void setDonorId(String donorId) {
		this.donorId = donorId;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public UserEntity getRequestor() {
		return requestor;
	}

	public void setRequestor(UserEntity requestor) {
		this.requestor = requestor;
	}

	public UserEntity getDonor() {
		return donor;
	}

	public void setDonor(UserEntity donor) {
		this.donor = donor;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	
	
}

package com.mphasis.healthappservice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_points")
public class UserPoints {
	
	@Id
	@Column(name="user_points_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="user_id")
	private String userId;
	
	@Column(name="co_relation_id")
	private Long coRelationId;
	
	@Column(name="category")
	private String category;
	
	@Column(name="points")
	private int points;
	
	@Column(name="added_on")
	private Date addedOn;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getCoRelationId() {
		return coRelationId;
	}

	public void setCoRelationId(Long coRelationId) {
		this.coRelationId = coRelationId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

}

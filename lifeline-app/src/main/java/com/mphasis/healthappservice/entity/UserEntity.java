package com.mphasis.healthappservice.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class UserEntity implements Serializable{
	
	@Id
	@Column(name="user_id")
	private String userId;
	
	@Column(name="full_name")
	private String fullName;
	
	@JsonIgnore
	@Column(name="password")
	private String password;
	
	@Column(name="email")
	private String email;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	@Column(name="blood_group")
	private String bloodGroup;
	
	@Column(name="is_admin")
	private boolean isAdmin;
	
	@Column(name="date_registered")
	private Date dateRegistered;
	
	@Column(name="profile_pic")
	private String profilePic;
	
	@Column(name="registration_confirmed")
	private boolean registrationConfirmed;
	
	@Column(name="location")
	private String location;
	@Transient
	private Integer points;
	

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Date getDateRegistered() {
		return dateRegistered;
	}

	public void setDateRegistered(Date dateRegistered) {
		this.dateRegistered = dateRegistered;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public boolean isRegistrationConfirmed() {
		return registrationConfirmed;
	}

	public void setRegistrationConfirmed(boolean registrationConfirmed) {
		this.registrationConfirmed = registrationConfirmed;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	
	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "UserEntity [userId=" + userId + ", fullName=" + fullName + ", password=" + password + ", email=" + email
				+ ", gender=" + gender + ", phoneNumber=" + phoneNumber + ", bloodGroup=" + bloodGroup + ", isAdmin="
				+ isAdmin + ", dateRegistered=" + dateRegistered + ", profilePic=" + profilePic
				+ ", registrationConfirmed=" + registrationConfirmed + "]";
	}



	
	
	
	
	
	

}

package com.mphasis.healthappservice.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "user_notification")
public class UserNotificationEntity {

	@EmbeddedId
	private UserNotificationPK id;

//	@ManyToOne
//	@MapsId("userId")
//	@JoinColumn(name = "user_id")
//	UserEntity user;
//
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "notification_id", insertable = false,updatable = false)
	NotificationEntity notification;

	@Column(name="is_read")
	private boolean read;

	public UserNotificationPK getId() {
		return id;
	}

	public void setId(UserNotificationPK id) {
		this.id = id;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}
	

	public NotificationEntity getNotification() {
		return notification;
	}

	public void setNotification(NotificationEntity notification) {
		this.notification = notification;
	}

	@Override
	public String toString() {
		return "UserNotificationEntity [id=" + id + ", read=" + read + "]";
	}

	
}

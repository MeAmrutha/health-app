package com.mphasis.healthappservice.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "notification")
public class NotificationEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="notification_id")
	private Long notificcationId;
	
	//@OneToMany(mappedBy = "notification")
	//Set<UserNotificationEntity> userNotifications;

	
	@Column(name="notification_title")
	private String title;
	
	@Column(name="notification_text")
	private String text;
	
	@Column(name="created_on")
	private Date postedOn;

	public Long getNotificcationId() {
		return notificcationId;
	}

	public void setNotificcationId(Long notificcationId) {
		this.notificcationId = notificcationId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getPostedOn() {
		return postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

//	public Set<UserNotificationEntity> getUserNotifications() {
//		return userNotifications;
//	}
//
//	public void setUserNotifications(Set<UserNotificationEntity> userNotifications) {
//		this.userNotifications = userNotifications;
//	}
//	
	
}

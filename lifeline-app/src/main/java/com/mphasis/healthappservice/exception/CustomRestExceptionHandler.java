package com.mphasis.healthappservice.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.mphasis.healthappservice.model.ApiError;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({BadCredentialsException.class, DisabledException.class})
	public final ResponseEntity<Object> handleUnAuthorizedExceptions(Exception ex, WebRequest request) {
		ex.printStackTrace();
		return new ResponseEntity(new ApiError(HttpStatus.UNAUTHORIZED.name(),
				ex.getMessage()), HttpStatus.UNAUTHORIZED);
	}
	
	
	@ExceptionHandler(ApiBadRequestException.class)
	public final ResponseEntity<Object> handleBadRequestExceptions(Exception ex, WebRequest request) {
		ex.printStackTrace();
		return new ResponseEntity(new ApiError(HttpStatus.BAD_REQUEST.name(),
				ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    
	    return new ResponseEntity<Object>(new ApiError(HttpStatus.BAD_REQUEST.name(),
				"Validation Error", errors), HttpStatus.BAD_REQUEST);
	}
	
}

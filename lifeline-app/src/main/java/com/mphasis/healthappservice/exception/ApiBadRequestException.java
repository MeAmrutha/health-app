package com.mphasis.healthappservice.exception;

public class ApiBadRequestException extends Exception {

	public ApiBadRequestException(String msg){
		super(msg);
	}
}

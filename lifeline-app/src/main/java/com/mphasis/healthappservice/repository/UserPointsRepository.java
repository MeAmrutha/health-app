package com.mphasis.healthappservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mphasis.healthappservice.entity.UserPoints;

@Repository
public interface UserPointsRepository extends JpaRepository<UserPoints, Long> {
	@Query("select sum(up.points) from UserPoints up where up.userId = :userId")
	public Integer getUserPoints(@Param("userId") String userId);
}

package com.mphasis.healthappservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mphasis.healthappservice.entity.UserNotificationEntity;
import com.mphasis.healthappservice.entity.UserNotificationPK;

@Repository
public interface UserNotificationRepository extends JpaRepository<UserNotificationEntity, UserNotificationPK> {

	public List<UserNotificationEntity> findByRead(boolean isRead);
	
	public List<UserNotificationEntity> findByIdUserIdOrderByIdNotificationIdDesc(String userId);
	
	public List<UserNotificationEntity> findByReadAndIdUserId(boolean isRead, String userId);
	
	@Modifying
	@Query("Update UserNotificationEntity un set read = true where un.id.userId = :userId and un.id.notificationId in (:ids)")
	public void markAsRead(@Param("userId") String userId, @Param("ids") List<Long> ids);
	
}

package com.mphasis.healthappservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mphasis.healthappservice.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {

	public List<UserEntity> findByBloodGroup(String bloodGroup);
	
//	@Query("Select u.userId from UserEntity u where u.bloodGroup = : bloodGroup")
//	public List<String> getUsersBybloodGroup(String bloodGroup);
}

package com.mphasis.healthappservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mphasis.healthappservice.entity.DonateRequestEntity;

@Repository
public interface DonateRequestRepository extends JpaRepository<DonateRequestEntity, Long> {

	@Query("SELECT dr from DonateRequestEntity dr where dr.isDeleted=false and dr.status='Pending' order by dr.requestedOn desc")
	public List<DonateRequestEntity> getActiveRequests();
	
	@Query("SELECT dr from DonateRequestEntity dr where dr.requestorId=:requestorId and dr.status!='Deleted' order by dr.lastModifiedOn desc")
	public List<DonateRequestEntity> getMyRequests(@Param("requestorId") String requestorId);
	
}

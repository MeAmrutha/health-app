package com.mphasis.healthappservice.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mphasis.healthappservice.config.JwtTokenUtil;
import com.mphasis.healthappservice.entity.UserEntity;
import com.mphasis.healthappservice.exception.ApiBadRequestException;
import com.mphasis.healthappservice.model.LoginRequest;
import com.mphasis.healthappservice.model.TokenResponse;
import com.mphasis.healthappservice.model.SignupRequest;
import com.mphasis.healthappservice.service.UserService;

@RestController
@RequestMapping("/api")
public class LoginController {

	private Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@GetMapping("/ping")
	public String ping() {
		return "Login Contoller";
	}

	@PostMapping("/login")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginRequest loginRequest) throws Exception {
		logger.debug("login api invoked Username   : {}", loginRequest.getUsername());
		authenticate(loginRequest.getUsername(), loginRequest.getPassword());

		final UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new TokenResponse(token));
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

	@PostMapping("/signup")
	public ResponseEntity<?> signup(@Valid @RequestBody SignupRequest signupRequest) throws Exception {
		TokenResponse response = new TokenResponse();
		UserEntity userEntity = userDetailsService.saveUser(signupRequest);
		if (userEntity != null) {
			final UserDetails userDetails = new User(userEntity.getUserId(), userEntity.getPassword(),
					new ArrayList<>());
			final String token = jwtTokenUtil.generateToken(userDetails);
			response.setToken(token);
		}
		return ResponseEntity.ok(response);
	}
}

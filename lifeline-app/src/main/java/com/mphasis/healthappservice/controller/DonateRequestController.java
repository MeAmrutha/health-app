package com.mphasis.healthappservice.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mphasis.healthappservice.entity.DonateRequestEntity;
import com.mphasis.healthappservice.entity.UserPoints;
import com.mphasis.healthappservice.model.ApiResponse;
import com.mphasis.healthappservice.model.UpdateStatusDto;
import com.mphasis.healthappservice.service.DonateRequestService;
import com.mphasis.healthappservice.service.UserPointService;

@RestController
@RequestMapping("/api/donate_request")
public class DonateRequestController {

	private Logger logger = LoggerFactory.getLogger(DonateRequestController.class);
	
	@Autowired
	private DonateRequestService donateRequestService;
	
	@GetMapping("/get")
	public List<DonateRequestEntity> getRequests(){
		return donateRequestService.getDonateRequests();
	}
	
	@PostMapping("/save")
	public ApiResponse saveRequest(@RequestBody DonateRequestEntity donateRequestEntity) {
		ApiResponse apiResponse = new ApiResponse();
		try {
			 donateRequestService.saveDonateRequest(donateRequestEntity);
			 apiResponse.setStatus(true);
			 apiResponse.setMessage("Raised your request successfully");
		}
		catch(Exception e){
			 apiResponse.setStatus(false);
			 apiResponse.setMessage("Failed raising your request");
		}
		return apiResponse;
	}
	
	@PostMapping("/update")
	public ApiResponse updateRequest(@RequestBody DonateRequestEntity donateRequestEntity) {
		ApiResponse apiResponse = new ApiResponse();
		try {
			 donateRequestService.updateDonateRequest(donateRequestEntity);
			 apiResponse.setStatus(true);
			 apiResponse.setMessage("Request updated successfully");
		}
		catch(Exception e){
			 apiResponse.setStatus(false);
			 apiResponse.setMessage("Failed to updated your request");
		}
		return apiResponse;
	}
	
	@GetMapping("/get_my_requests")
	public List<DonateRequestEntity> getRequestsByRequester(Principal principal){
		return donateRequestService.getDonateRequestsByRequestorId(principal.getName());
	}
	
	@PostMapping("/assign_donor")
	public ApiResponse assignDonor(@RequestBody Map<String,String> request) {
		ApiResponse apiResponse = new ApiResponse();
		try {
			 donateRequestService.assignDonor(request);
			 apiResponse.setStatus(true);
			 apiResponse.setMessage("Donor assigned successfully");
		}
		catch(Exception e){
			 apiResponse.setStatus(false);
			 apiResponse.setMessage("Failed assigning donor");
		}
		return apiResponse;
	}
	
	@DeleteMapping("/deleteRequest/{donate_request_id}")
	public ApiResponse deleteRequest(@PathVariable("donate_request_id") Long donateRequestId){
		ApiResponse apiResponse = new ApiResponse();
		try {
			 donateRequestService.deleteRequest(donateRequestId);
			 apiResponse.setStatus(true);
			 apiResponse.setMessage("Request deleted successfully");
		}
		catch(Exception e){
			 apiResponse.setStatus(false);
			 apiResponse.setMessage("Failed deleting the request");
		}
		return apiResponse;
	}
	
	@PostMapping("/updateStatus")
	public ApiResponse updateStatus(@RequestBody UpdateStatusDto updateStatusDto){
		ApiResponse apiResponse = new ApiResponse();
		try {
			 donateRequestService.updateStatus(updateStatusDto);
			 apiResponse.setStatus(true);
			 apiResponse.setMessage("Updated status successfully");
		}
		catch(Exception e){
			 apiResponse.setStatus(false);
			 apiResponse.setMessage("Failed updating status");
		}
		return apiResponse;
	}
	
} 

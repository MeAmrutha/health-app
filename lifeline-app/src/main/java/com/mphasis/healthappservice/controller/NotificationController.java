package com.mphasis.healthappservice.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mphasis.healthappservice.entity.UserNotificationEntity;
import com.mphasis.healthappservice.model.ApiResponse;
import com.mphasis.healthappservice.service.NotificationService;

@RestController
@RequestMapping("/api/notification")
public class NotificationController {
	
	@Autowired
	private NotificationService notificationService; 
	
	@GetMapping("/get")
	public List<UserNotificationEntity> getNotifications(Principal principal) {
		return notificationService.getNotifications(principal.getName());
	}
	
	@GetMapping("/getUnread")
	public List<UserNotificationEntity> getUnReadNotifications(Principal principal) {
		return notificationService.getUnReadNotifications(principal.getName());
	}
	
	@PostMapping("/markAsRead")
	public ResponseEntity<?> markAsRead(@RequestBody List<Long> notificationIds, Principal principal){
		ApiResponse apiResponse = new ApiResponse();
		try {
			notificationService.markAsRead(principal.getName(), notificationIds);
			apiResponse.setStatus(true);
			apiResponse.setMessage("Succcess");
		}
		catch (Exception e) {
			apiResponse.setStatus(false);
			apiResponse.setMessage("Error "+ e.getMessage());
		}
		return ResponseEntity.ok(apiResponse);
	}
	
	
}

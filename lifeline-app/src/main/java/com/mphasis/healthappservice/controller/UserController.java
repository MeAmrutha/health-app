package com.mphasis.healthappservice.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mphasis.healthappservice.entity.UserEntity;
import com.mphasis.healthappservice.exception.ApiBadRequestException;
import com.mphasis.healthappservice.model.SignupRequest;
import com.mphasis.healthappservice.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	private UserService userDetailsService;
	
	@GetMapping("/get")
	public UserEntity getUser(Principal principal) {
		return userDetailsService.getUser(principal.getName());
	}
	

	@GetMapping("/get_all")
	public List<UserEntity> getUser() {
		return userDetailsService.getUsers();
	}
	
	@PostMapping("/update")
	public UserEntity updateUser(@RequestBody SignupRequest updateRequest) throws ApiBadRequestException {
		return userDetailsService.updateUser(updateRequest);
	}
}

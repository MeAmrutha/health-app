package com.mphasis.healthappservice.model;

public class TokenResponse {

	private String token;
	
	public TokenResponse(String token) {
		super();
		this.token = token;
	}

	public TokenResponse() {
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	
}

package com.mphasis.healthappservice.model;

import java.util.List;
import java.util.Map;

public class ApiError {
	private String status;
	private String message;
	private Map<String, String> errors;
	
	public ApiError(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	public ApiError(String status, String message, Map<String, String> errors) {
		super();
		this.status = status;
		this.message = message;
		this.errors = errors;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Map<String, String> getErrors() {
		return errors;
	}
	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}
	
}

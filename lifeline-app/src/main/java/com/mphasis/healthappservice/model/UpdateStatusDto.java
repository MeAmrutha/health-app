package com.mphasis.healthappservice.model;

public class UpdateStatusDto {

	private Long donateRequestId;
	private String status;
	
	public Long getDonateRequestId() {
		return donateRequestId;
	}
	public void setDonateRequestId(Long donateRequestId) {
		this.donateRequestId = donateRequestId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}

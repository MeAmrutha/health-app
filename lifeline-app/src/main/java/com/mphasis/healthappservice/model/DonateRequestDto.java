package com.mphasis.healthappservice.model;

import java.util.Date;

import javax.persistence.Column;

public class DonateRequestDto {

	private Long donateRequestId;
	private String requestorId;
	private UserInfoDto userInfoDto;
	private String requestFor;
	private String requestCategory;
	private String description;
	private String additionalInfo;
	private String location;
	private String status;
	private Date requestedOn;
	private String donorId;
	public Long getDonateRequestId() {
		return donateRequestId;
	}
	public void setDonateRequestId(Long donateRequestId) {
		this.donateRequestId = donateRequestId;
	}
	public String getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}
	public UserInfoDto getUserInfoDto() {
		return userInfoDto;
	}
	public void setUserInfoDto(UserInfoDto userInfoDto) {
		this.userInfoDto = userInfoDto;
	}
	public String getRequestFor() {
		return requestFor;
	}
	public void setRequestFor(String requestFor) {
		this.requestFor = requestFor;
	}
	public String getRequestCategory() {
		return requestCategory;
	}
	public void setRequestCategory(String requestCategory) {
		this.requestCategory = requestCategory;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getRequestedOn() {
		return requestedOn;
	}
	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}
	public String getDonorId() {
		return donorId;
	}
	public void setDonorId(String donorId) {
		this.donorId = donorId;
	}
	@Override
	public String toString() {
		return "DonateRequestDto [donateRequestId=" + donateRequestId + ", requestorId=" + requestorId
				+ ", userInfoDto=" + userInfoDto + ", requestFor=" + requestFor + ", requestCategory=" + requestCategory
				+ ", description=" + description + ", additionalInfo=" + additionalInfo + ", location=" + location
				+ ", status=" + status + ", requestedOn=" + requestedOn + ", donorId=" + donorId + "]";
	}
	
	
}

package com.mphasis.healthappservice.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class SignupRequest {

	@NotBlank(message = "User Id is required")
	private String userId;
	private String fullName;
	
	@Email(message = "Email should be valid")
	private String email;
	private String location;
	private String phoneNumber;
	private String bloodGroup;
	private String password;
	private String gender;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	@Override
	public String toString() {
		return "SignupRequest [userId=" + userId + ", fullName=" + fullName + ", email=" + email + ", location="
				+ location + ", phoneNumber=" + phoneNumber + ", bloodGroup=" + bloodGroup + ", password=" + password
				+ ", gender=" + gender + "]";
	}

	
	
}

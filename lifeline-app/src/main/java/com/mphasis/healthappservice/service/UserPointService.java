package com.mphasis.healthappservice.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mphasis.healthappservice.entity.DonateRequestEntity;
import com.mphasis.healthappservice.entity.UserPoints; 
import com.mphasis.healthappservice.repository.UserPointsRepository;

@Service
public class UserPointService {
	
	@Autowired
	private UserPointsRepository userPointsRepository;
	
	public Integer getPoints(String userId) {
		return userPointsRepository.getUserPoints(userId);
	}
	
	public void saveUserPoints(DonateRequestEntity donateRequestEntity ) {
		UserPoints userPoints = preapareUserPointsEntity(donateRequestEntity);
		userPointsRepository.save(userPoints);
	}
	
	public UserPoints preapareUserPointsEntity(DonateRequestEntity donateRequestEntity) {
		UserPoints userPoints = new UserPoints();
		userPoints.setAddedOn(new Date());
		userPoints.setCategory("Donation");
		userPoints.setCoRelationId(donateRequestEntity.getDonateRequestId());
		userPoints.setUserId(donateRequestEntity.getDonorId());
		if(donateRequestEntity.getRequestCategory().equalsIgnoreCase("Blood")) {
			userPoints.setPoints(150);
		}
		else {
			userPoints.setPoints(100);
		}
		return userPoints;
	}
	
	
}

package com.mphasis.healthappservice.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mphasis.healthappservice.entity.DonateRequestEntity;
import com.mphasis.healthappservice.model.UpdateStatusDto;
import com.mphasis.healthappservice.repository.DonateRequestRepository;
import com.mphasis.healthappservice.repository.UserPointsRepository;

@Service
public class DonateRequestService {
	
	@Autowired
	private DonateRequestRepository donateRequestRepository;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private UserPointService userPointService;
	
	@Autowired
	private UserService userService;
	
	public List<DonateRequestEntity> getDonateRequests(){
		return donateRequestRepository.getActiveRequests();
	}

	public DonateRequestEntity saveDonateRequest(DonateRequestEntity donateRequestEntity) {
		donateRequestEntity.setRequestedOn(new Date());
		donateRequestEntity.setLastModifiedOn(new Date());
		donateRequestEntity = donateRequestRepository.save(donateRequestEntity);
		donateRequestEntity.setRequestor(userService.getUser(donateRequestEntity.getRequestorId()));
		notificationService.postNotifications(donateRequestEntity);
		return donateRequestEntity;
	}
	
	public void updateDonateRequest(DonateRequestEntity donateRequestEntity) {
		Optional<DonateRequestEntity> requestOpt = donateRequestRepository.findById(donateRequestEntity.getDonateRequestId());
		if(requestOpt.isPresent()) {
			DonateRequestEntity currentDonateRequestEntity = requestOpt.get();
			currentDonateRequestEntity.setRequestFor(donateRequestEntity.getRequestFor());
			currentDonateRequestEntity.setRequestedBloodGroup(donateRequestEntity.getRequestedBloodGroup());
			currentDonateRequestEntity.setRequestorPhNum(donateRequestEntity.getRequestorPhNum());
			currentDonateRequestEntity.setRequestCategory(donateRequestEntity.getRequestCategory());
			currentDonateRequestEntity.setDescription(donateRequestEntity.getDescription());
			currentDonateRequestEntity.setLocation(donateRequestEntity.getLocation());
			currentDonateRequestEntity.setLastModifiedOn(new Date());
			currentDonateRequestEntity = donateRequestRepository.save(currentDonateRequestEntity);
			if(!currentDonateRequestEntity.getRequestCategory().equals(donateRequestEntity.getRequestCategory()) ||
					!currentDonateRequestEntity.getRequestedBloodGroup().equals(donateRequestEntity.getRequestedBloodGroup()))
			{
				currentDonateRequestEntity.setRequestor(userService.getUser(donateRequestEntity.getRequestorId()));
				notificationService.postNotifications(currentDonateRequestEntity);
			}
		}
	}
	
	
	public DonateRequestEntity updateStatus(DonateRequestEntity donateRequestEntity) {
		return donateRequestRepository.save(donateRequestEntity);
	}
	
	public List<DonateRequestEntity> getDonateRequestsByRequestorId(String requesterId){
		return donateRequestRepository.getMyRequests(requesterId);
	}
	
	public void assignDonor(Map<String,String> request){
		Long requestId = Long.valueOf(request.get("donateRequestId"));
		String donorId  = request.get("donorId");
		Optional<DonateRequestEntity> donateRequestEntityOpt = donateRequestRepository.findById(requestId);
		if(donateRequestEntityOpt.isPresent()) {
			DonateRequestEntity donateRequestEntity = donateRequestEntityOpt.get();
			donateRequestEntity.setDonorId(donorId);
			donateRequestEntity.setStatus("Accepted");
			donateRequestEntity.setLastModifiedOn(new Date());
			donateRequestRepository.save(donateRequestEntity);
			notificationService.postNotifications(donateRequestEntity);
		}

	}

	public void deleteRequest(Long donateRequestId) {
		donateRequestRepository.deleteById(donateRequestId);
	}

	public void updateStatus(UpdateStatusDto updateStatusDto) {
		Long donateRequestId = updateStatusDto.getDonateRequestId();
		String status  = updateStatusDto.getStatus();		
		
		Optional<DonateRequestEntity> donateRequestEntityOpt = donateRequestRepository.findById(donateRequestId);
		if(donateRequestEntityOpt.isPresent()) {
			DonateRequestEntity donateRequestEntity = donateRequestEntityOpt.get();
			donateRequestEntity.setStatus(status);
			if(status.equalsIgnoreCase("Completed")) {
				userPointService.saveUserPoints(donateRequestEntity);
				notificationService.postNotifications(donateRequestEntity);
			}
			if(status.equalsIgnoreCase("Pending")) {
				donateRequestEntity.setDonorId(null);
				donateRequestEntity.setDonor(null);
			}
			donateRequestEntity.setLastModifiedOn(new Date());
			donateRequestRepository.save(donateRequestEntity);
		}
		
	}
	
	
	
}

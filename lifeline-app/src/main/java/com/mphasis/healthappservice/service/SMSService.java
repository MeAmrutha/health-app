package com.mphasis.healthappservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class SMSService {
	private Logger logger = LoggerFactory.getLogger(SMSService.class);
	
	private String SMS_API_KEY = "mblFPKsxI3Q2jnoZaz9WB541gASVrvEcHdUhJfpGueR7ikTtqCPpeDw64UBntclMyX5WhZfbaTuLR3Q0";

	public void sendSMS(List<String> variableList,List<String> phNumList) {
		String phNums = phNumList.stream().collect(Collectors.joining(","));
		String variables = variableList.stream().collect(Collectors.joining("|"));
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("authorization", SMS_API_KEY);
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
				
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("sender_id","SMSIND");
		map.add("message","36278");
		map.add("language","english");
		map.add("numbers",phNums);
		map.add("route","qt");
		map.add("flash","0");
		map.add("variables","{#EE#}|{#AA#}|{#EE#}|{#FF#}|{#BB#}");
		map.add("variables_values",variables);
		logger.info("SMS request :"+ map);
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
		String url = "https://www.fast2sms.com/dev/bulk";
		String response = new RestTemplate().exchange(url, HttpMethod.POST, entity, String.class).getBody();
		logger.info("SMS response :"+ response);
	}
}

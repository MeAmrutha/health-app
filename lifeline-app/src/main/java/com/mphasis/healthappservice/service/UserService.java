package com.mphasis.healthappservice.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mphasis.healthappservice.entity.UserEntity;
import com.mphasis.healthappservice.exception.ApiBadRequestException;
import com.mphasis.healthappservice.model.SignupRequest;
import com.mphasis.healthappservice.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserPointService userPointService;
	
	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<UserEntity> userEntityOpt = userRepository.findById(username);
		if(userEntityOpt.isPresent()) {
			UserEntity userEntity = userEntityOpt.get();
			return new User(userEntity.getUserId(), userEntity.getPassword(),new ArrayList<>());
		}
		else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
	
	public UserEntity saveUser(SignupRequest signupRequest) throws ApiBadRequestException {
		Optional<UserEntity> userEntityOpt = userRepository.findById(signupRequest.getUserId());
		if(userEntityOpt.isPresent()) {
			throw new ApiBadRequestException("User already present with user id : "+ signupRequest.getUserId());
		}
		return userRepository.save(prepareUserEntity(signupRequest));
	}

	private UserEntity prepareUserEntity(SignupRequest signupRequest) {
		UserEntity userEntity = new UserEntity();
		userEntity.setUserId(signupRequest.getUserId());
		userEntity.setDateRegistered(new Date());
		userEntity.setEmail(signupRequest.getEmail());
		userEntity.setGender(signupRequest.getGender());
		userEntity.setFullName(signupRequest.getFullName());
		userEntity.setPhoneNumber(signupRequest.getPhoneNumber());
		userEntity.setLocation(signupRequest.getLocation());
		userEntity.setPassword(passwordEncoder.encode(signupRequest.getPassword()));
		userEntity.setRegistrationConfirmed(true);
		userEntity.setAdmin(false);
		userEntity.setBloodGroup(signupRequest.getBloodGroup());
		return userEntity;
	}
	
	public UserEntity getUser(String userId){
		UserEntity userEntity = null;
		userEntity = userRepository.findById(userId).orElse(null);
		if(userEntity != null) {
			try {
			userEntity.setPoints(userPointService.getPoints(userId));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return userEntity;
	}
	
	public List<UserEntity> getUsers(){
		return userRepository.findAll();
	}
	
	public List<UserEntity> getUsersByBlood(String bloodGroup){
		return userRepository.findByBloodGroup(bloodGroup);
	}

	public UserEntity updateUser(SignupRequest updateRequest) throws ApiBadRequestException {
		Optional<UserEntity> userEntityOpt = userRepository.findById(updateRequest.getUserId());
		if(!userEntityOpt.isPresent()) {
			throw new ApiBadRequestException("user not found with Id "+ updateRequest.getUserId());
		}
		UserEntity userEntity = userEntityOpt.get();
		userEntity.setEmail(updateRequest.getEmail());
		userEntity.setGender(updateRequest.getGender());
		userEntity.setFullName(updateRequest.getFullName());
		userEntity.setPhoneNumber(updateRequest.getPhoneNumber());
		userEntity.setLocation(updateRequest.getLocation());
		userEntity.setBloodGroup(updateRequest.getBloodGroup());
		userEntity = userRepository.save(userEntity);
		try {
			userEntity.setPoints(userPointService.getPoints(userEntity.getUserId()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return userEntity;
	}
}

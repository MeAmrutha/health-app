package com.mphasis.healthappservice.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.mphasis.healthappservice.entity.DonateRequestEntity;
import com.mphasis.healthappservice.entity.NotificationEntity;
import com.mphasis.healthappservice.entity.UserEntity;
import com.mphasis.healthappservice.entity.UserNotificationEntity;
import com.mphasis.healthappservice.entity.UserNotificationPK;
import com.mphasis.healthappservice.repository.NotificationRepository;
import com.mphasis.healthappservice.repository.UserNotificationRepository;

@Service
@Transactional
public class NotificationService {

	@Autowired
	private NotificationRepository notificationRepository;
	
	@Autowired
	private UserNotificationRepository userNotificationRepository; 
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SMSService smsService;
	
	public void postNotifications(DonateRequestEntity donateRequestEntity) {
		String bloodGroup = donateRequestEntity.getRequestedBloodGroup();
		String category = donateRequestEntity.getRequestCategory();
		String name = donateRequestEntity.getRequestor().getFullName();
		String phNum = donateRequestEntity.getRequestorPhNum();
		String loc = donateRequestEntity.getLocation();
		String status = donateRequestEntity.getStatus();
		
		UserEntity donor = donateRequestEntity.getDonor();
		if(donor == null && donateRequestEntity.getDonorId() != null) {
			donor = userService.getUser(donateRequestEntity.getDonorId());
		}
		List<String> userIdList = null;
		List<String> userPhList = null;
		String title = null;
		String text = null;
		List<String> textVariablesForMsg = new ArrayList<String>();
		
		if(status.equalsIgnoreCase("Accepted")) {
			userIdList = Arrays.asList(donateRequestEntity.getRequestorId());
			userPhList = Arrays.asList(donateRequestEntity.getRequestorId());
			title = "Your " + bloodGroup + " " + category + " request is accepted" ;
			text =  "Hey " +name + "! " +"Your request has been accepted by " + donor.getFullName() + "." + 
							" Pls check your raised request for donor details or contact the donor: " + donor.getPhoneNumber() ;
			textVariablesForMsg.add(name);
			textVariablesForMsg.add(donor.getFullName());
			textVariablesForMsg.add(donor.getPhoneNumber());
		}
		if(status.equalsIgnoreCase("Completed")) {
			userIdList = Arrays.asList(donor.getUserId());
			userPhList = Arrays.asList(donor.getUserId());
			title = "Thanks " + donor.getFullName() + " for your donation!";
			text =  "We appreciate your help for donating " + bloodGroup + " " + category + " to " + name;

			textVariablesForMsg.add(bloodGroup);
			textVariablesForMsg.add(category);
			textVariablesForMsg.add(name);
		}
		
		if(status.equalsIgnoreCase("Pending"))  {
			List<UserEntity> users = userService.getUsersByBlood(bloodGroup);
			if(!CollectionUtils.isEmpty(users)) {
				userIdList = users.stream().map(p->p.getUserId()).collect(Collectors.toList());
				userPhList = users.stream().map(p->p.getPhoneNumber()).collect(Collectors.toList());
				//stop notification for requester
				userIdList.remove(donateRequestEntity.getRequestorId());
				title = "Request for " + bloodGroup + " " + category;
				text = name + " is requesting for "  + bloodGroup + " " + category + " at " +loc+ " location. " + "To donate pls contact: " + phNum;	
				textVariablesForMsg.add(name);
				textVariablesForMsg.add(bloodGroup);
				textVariablesForMsg.add(category);
				textVariablesForMsg.add(loc);
				textVariablesForMsg.add(phNum);
			}
		}
		if(!CollectionUtils.isEmpty(userIdList)) {
			postNotification(title, text, userIdList);
			//smsService.sendSMS(textVariablesForMsg,userPhList);
		}
	}
	
	
	public void postNotification(String title, String text, List<String> userList) {
		NotificationEntity entity = new NotificationEntity();
		entity.setText(text);
		entity.setTitle(title);
		entity.setPostedOn(new Date());
		entity = notificationRepository.save(entity);
		
		Set<UserNotificationEntity> userNotifications = new HashSet<>();
		for(String userId : userList) {
			UserNotificationEntity userNotificationEntity = new UserNotificationEntity();
			userNotificationEntity.setId(new UserNotificationPK(entity.getNotificcationId(), userId));
			userNotificationEntity.setRead(false);
			userNotifications.add(userNotificationEntity);
		}
		userNotificationRepository.saveAll(userNotifications);
	}
	
	public List<UserNotificationEntity> getNotifications(String userId){
		return userNotificationRepository.findByIdUserIdOrderByIdNotificationIdDesc(userId);
	}
	
	public List<UserNotificationEntity> getUnReadNotifications(String userId){
		return userNotificationRepository.findByReadAndIdUserId(false, userId);
	}
	
	public void markAsRead(String userId, List<Long> ids) {
		userNotificationRepository.markAsRead(userId, ids);
	}
}

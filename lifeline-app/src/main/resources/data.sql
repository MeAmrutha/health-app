INSERT INTO USERS(user_id,full_name,password,email,gender,location,blood_group,is_admin,date_registered,phone_number,registration_confirmed)
VALUES('2360416','Satya Talatam', '$2a$10$.FDfSRBX5bKlpsXL0scdH.cXUr52Y2s78RR14Q3XHlzYf8dMvewzq','talatam.s@mphasis.com','Male', 'Hyderabad','A-',0,sysdate(),'9492939190',1);

INSERT INTO USERS(user_id,full_name,password,email,gender,location,blood_group,is_admin,date_registered,phone_number,registration_confirmed)
VALUES('2354966','Amrutha Vadapalli', '$2a$10$.FDfSRBX5bKlpsXL0scdH.cXUr52Y2s78RR14Q3XHlzYf8dMvewzq','amrutha.vadapalli@mphasis.com','Female', 'Pune','O+',0,sysdate(),'9492939190',1);


INSERT INTO USERS(user_id,full_name,password,email,gender,location,blood_group,is_admin,date_registered,phone_number,registration_confirmed)
VALUES('2354965','Sowmya Paluru', '$2a$10$.FDfSRBX5bKlpsXL0scdH.cXUr52Y2s78RR14Q3XHlzYf8dMvewzq','paluru.sowmya@mphasis.com','Female', 'Bangalore','B+',0,sysdate(),'9492939190',1);


INSERT INTO USERS(user_id,full_name,password,email,gender,location,blood_group,is_admin,date_registered,phone_number,registration_confirmed)
VALUES('2356223','Sudha Pallam', '$2a$10$.FDfSRBX5bKlpsXL0scdH.cXUr52Y2s78RR14Q3XHlzYf8dMvewzq','sudhasrinivas.pallam@mphasis.com','Female', 'Hyderabad','B+',0,sysdate(),'9492939190',1);


INSERT INTO donate_requests(
donate_request_id
	requestor_id,
	request_for,
	request_category,
	requested_blood_group,
	description,
	status,
	location,
	requested_on,
	donor_id,
	is_active,
	is_deleted,
	last_updated_on,
	requestor_phone_number)
	values(1,"2354966","Myself","Blood","A-","Need A- blood urgently","Pending","Hyderabad",sysdate(),null,1,0,sysdate(),"9492939190");
	
	INSERT INTO donate_requests(
	donate_request_id,
	requestor_id,
	request_for,
	request_category,
	requested_blood_group,
	description,
	status,
	location,
	requested_on,
	donor_id,
	is_active,
	is_deleted,
	last_updated_on,
	requestor_phone_number)
	values(2,"2354965","Others","Plasma","O+","Need A- Plasma urgently","Pending","Hyderabad",sysdate(),null,1,0,sysdate(),"9492939190");
	
INSERT INTO donate_requests(
donate_request_id,
	requestor_id,
	request_for,
	request_category,
	requested_blood_group,
	description,
	status,
	location,
	requested_on,
	donor_id,
	is_active,
	is_deleted,
	last_updated_on,
	requestor_phone_number)
	values(3,"2360418","Myself","Covid Recovered Plasma","B+","Need O+ Covid Recovered Plasma urgently","Pending","Hyderabad",sysdate(),null,1,0,sysdate(),"9492939190");
	

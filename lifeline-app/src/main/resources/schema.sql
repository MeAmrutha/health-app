CREATE TABLE IF NOT EXISTS users (
  user_id varchar(20) NOT NULL,
  full_name varchar(255) NOT NULL,
  password varchar(100) NOT NULL,
  email varchar(55) NOT NULL,
  gender varchar(20) NOT NULL,
  location varchar(255) NOT NULL,
  blood_group varchar(10),
  is_admin tinyint(1),
  date_registered datetime,
  profile_pic varchar(200),
  phone_number varchar(10) not null,
  registration_confirmed tinyint(1),
  PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS donate_requests(
	donate_request_id bigint not null auto_increment,
	requestor_id varchar(20) NOT NULL,
	request_for varchar(100) not null,
	request_category varchar(50) not null,
	requested_blood_group varchar(10) not NULL,
	description varchar(500),
	additional_info text,
	status varchar(20),
	location varchar(100),
	requested_on datetime,
	donor_id varchar(20),
	is_active tinyint(1),
	is_deleted tinyint(1),
	last_updated_on datetime,
	requestor_phone_number varchar(10) not null, 
	PRIMARY KEY (donate_request_id),
	foreign key (requestor_id) references users(user_id),
	foreign key (donor_id) references users(user_id)
);

CREATE TABLE IF NOT EXISTS comments(
	comment_id bigint not null auto_increment,
	corelation_id varchar(200) not null,
	comment_text text,
	commented_at datetime,
	is_deleted tinyint,
	PRIMARY KEY (comment_id)
);

CREATE TABLE IF NOT EXISTS notification(
notification_id bigint not null auto_increment,
notification_title varchar(500),
notification_text varchar(500),
created_on datetime,
PRIMARY KEY (notification_id)
);

CREATE TABLE IF NOT EXISTS user_notification(
	notification_id bigint NOT NULL,
	user_id varchar(20) NOT NULL,
	is_read tinyint(1),
	foreign key (notification_id) references notification(notification_id)
);

CREATE TABLE IF NOT EXISTS user_points(
	user_points_id bigint not null auto_increment,
	user_id varchar(20) NOT NULL,
	co_relation_id bigint not null,
	category varchar(20),
	points int NOT NULL,
	added_on datetime,
	PRIMARY KEY (user_points_id)
);

